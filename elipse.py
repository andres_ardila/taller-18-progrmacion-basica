import numpy as np
import matplotlib.pyplot as plt
import math

u=1.  
v=0.5  
a=2.   
b=1.5 

t = np.linspace(0, 2*math.pi, 100) 
plt.plot(u+a*np.cos(t) , v+b*np.sin(t)) 
plt.grid(color='lightgray') 
plt.show() 